from flask import Flask

app = Flask(__name__)


@app.route("/")
def main():
    response = {
        'message': 'Welcome to the BEON Python/Django Challenge'
    }
    return response, 200


@app.route("/users")
def users():
    response = {
        'message': 'Nothing build yet.'
    }
    return response, 404


@app.route("/categories")
def categories():
    response = {
        'message': 'Nothing build yet.'
    }
    return response, 404


if __name__ == "main":
    app.run(host="0.0.0.0", port=5000)
