import pytest

from app import app



@pytest.fixture
def client():
    app.config["TESTING"] = True

    yield app.test_client()  # tests run here

def test_main(client):
    response = client.get("/", content_type="html/text")
    assert response.status_code == 200

